Surveillez l'utilisation de vos conteneurs à l'aide de Grafana et de tout outil de surveillance des systèmes nécessaire permettant de collecter des métriques de vos conteneurs. 

Bien évidement, tout cela devra être conteneurisé ! 

___ 

Cet exercice n'est pas guidé volontairement. A vous de voir l'existant sur Internet (vous trouverez aisément des TP/des articles/des guides permettant de réaliser ça). 

L'objectif ici est de mettre les mains dans le cambouis : mettez en avant dans un document .md les difficultés rencontrées, ce que vous avez fait pour déterminer l'origine des problèmes, et ce que vous avez réalisé pour débug les différents soucis.

##################### Réponse ########################

Il faut lancer la commande "#>docker compose -f compose.yml up -d"

Difficulté sur la nécessité d'ajouter le paramètre "user: root" sur le container prometheus car la copie du fichier de configuration ne disposais pas des droits nécessaires.


